package org.libreflix.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import org.libreflix.android.R;

public class MainActivity extends AppCompatActivity {


    WebView webview;
    LinearLayout view_sem_acesso;
    ImageView img_icon;

    final String LIBREFLIX_URL = "https://libreflix.org";

    boolean first_access = true;
    boolean erro =false;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        view_sem_acesso = findViewById(R.id.view_sem_acesso);
        img_icon = findViewById(R.id.img_icon);
        webview = findViewById(R.id.web_view);

        webview.setWebViewClient(new LibreFlixWebViewClient());

        WebSettings settings = webview.getSettings();
        settings.setMediaPlaybackRequiresUserGesture(false);
        settings.setJavaScriptEnabled(true);

        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setScrollbarFadingEnabled(false);


        if(!isNetworkAvailable()){

            webview.setVisibility(View.GONE);
            view_sem_acesso.setVisibility(View.VISIBLE);

        }else {

            webview.loadUrl(LIBREFLIX_URL);
        }
    }


    @Override
    public void onBackPressed() {

        if (webview.canGoBack()) {
            webview.goBack();
            return;
        }
        super.onBackPressed();
    }

    public class LibreFlixWebViewClient extends WebViewClient {


        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            Log.d("ERRO", error.toString());
            erro = true;
            img_icon.setVisibility(View.GONE);
            view_sem_acesso.setVisibility(View.VISIBLE);
            webview.setVisibility(View.GONE);

        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            if(first_access){
                startAnimation();
            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            stopAnimation();

            if(first_access){
                webview.setVisibility(View.VISIBLE);
                first_access = false;
            }else if(!erro){

                if(webview.getVisibility()==View.GONE){
                    webview.setVisibility(View.VISIBLE);
                }

            }


        }
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void refreshPage(View view) {

        erro=false;
        startAnimation();
        view_sem_acesso.setVisibility(View.GONE);

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                SystemClock.sleep(2000);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                stopAnimation();
                img_icon.setVisibility(View.GONE);

                if(!isNetworkAvailable()){

                    webview.setVisibility(View.GONE);
                    view_sem_acesso.setVisibility(View.VISIBLE);

                }else {

                    webview.loadUrl(LIBREFLIX_URL);
                }
            }
        }.execute();
    }


    public void startAnimation(){
        img_icon.setVisibility(View.VISIBLE);
        final Animation animation = new AlphaAnimation(1,0);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        img_icon.startAnimation(animation);
    }

    public void stopAnimation(){
        img_icon.clearAnimation();
    }

}
